# OSRS Fishing Grind Buddy



## What is this?

The OSRS Fishing Grind Buddy is a tool to view the current trade price and trading trend of higher level fish. This is useful if you're trying to get that Fishing Skillcape while also making GP!

## Why?

I have wanted to get more experience in using API's and in general software programming, so this project is an exercise in learning. Hopefully this will become a portfolio piece for me and it will be useful for people outside of myself!

## Tools In Use

- [ ] I am working on this project in Visual Studio 2022. The current testing project is a regular C# Console App.
- [ ] The Official Runescape GE API is in use to retrieve item information
- [ ] I am using an external file to parse the JSON data that is returned from the API calls (thank you Bunny83, oPless, and subnomo)
- [ ] On mobile devices (Android, iOS) there are push notifications sent through Google Firebase Cloud Messaging service

## Plans

There is a basic console app version of this in this project! It has less features then the MAUI created app

So far in the MAUI app I have implemented the UI loading from the C# file instead of a XAML file. This is done because the data from the grand exchange API must be retrieved before fully loading the UI. The basic data from the console app is in the app along with images for each fish! Each fish can also be added to a "Watchlist". This saves the ID of the fish to be used for a report notification feature.

Originally, I had a goal of being able to chart a comparison between raw fish and cooked fish. However, after some looking around, I haven't found a tool that works all that well with MAUI or shows much promise. I did try one but it was quite a disaster! If anyone has ideas or guidance please feel free to contact me.

### Goals
- [ ] Release app on itch.io and maybe other places like app stores for people to download this!

### Platforms

I would like to make this app available on Windows, Android, and iOS.