﻿using SimpleJSON;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.XPath;

namespace TestingAPI
{
    public class RS_Item
    {
        public enum ItemCategory
        {
            Misc = 0,
            Ammo = 1,
            Arrows = 2,
            Bolts = 3,
            ConstructionMats = 4,
            ConstructionProd = 5,
            CookingIngredients = 6,
            Costumes = 7,
            CraftingMats = 8,
            Familiars = 9,
            FarmProduce = 10,
            FletchingMats = 11,
            FoodDrink = 12,
            HerbloreMats = 13,
            HuntingEquipment = 14,
            HuntingProd = 15,
            Jewellery = 16,
            MageArmor = 17,
            MageWeapons = 18,
            MeleeAR_Low = 19,
            MeleeAR_Mid = 20,
            MeleeAR_High = 21,
            MeleeWP_Low = 22,
            MeleeWP_Mid = 23,
            MeleeWP_High = 24,
            MiningSmithing = 25,
            Potions = 26,
            PrayerArmor = 27,
            PrayerMats = 28,
            RangeArmor = 29,
            RangeWeapons = 30,
            Runecrafting = 31,
            RuneSpellTele = 32,
            Seeds = 33,
            SummonScrolls = 34,
            ToolsContainers = 35,
            WoodcuttingProd = 36,
            PocketItems = 37,
            StoneSpirits = 38,
            Salvage = 39,
            FiremakingProd = 40,
            ArchaeologyMats = 41,
            WoodSpirits = 42,
            NecroArmor = 43
        }
        private string name, description;
        private ItemCategory type;
        private int price, ID;
        private float day30trade;
        public float XP;
        public int LevelRequirement;
        public RS_Item(string newName, string newDesc,  ItemCategory typeID, int newPrice, int newID, float day30) { 
            name = newName;
            description = newDesc;
            type = typeID;
            price = newPrice;
            ID = newID;
            day30trade = day30;
            
        }

        public RS_Item(JSONNode json)
        {
            name = json["item"]["name"];
            description = json["item"]["description"];
            Enum.TryParse(json["item"]["type"], out type);
            price = ParsePrice(json["item"]["current"]["price"]);
            ID = int.Parse(json["item"]["id"]);
            day30trade = Parse30Day(json["item"]["day30"]["change"]);
        }

        public string Name()
        {
            return name;
        }
        public string Description()
        {
            return description;
        }
        public ItemCategory Type()
        {
            return type;
        }
        public int Price()
        {
            return price;
        }

        public int GetID()
        {
            return ID;
        }
        public float Day30trade()
        {
            return day30trade;
        }

        public void DisplayItemInfo()
        {
            //prints the data in a cool and nice way
            Console.WriteLine("\n");
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine(name);
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(description);
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("XP Gained: " + XP);
            Console.WriteLine("Level Required: " + LevelRequirement + "\n");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Trading:");
            Console.Write("Current Price: "+price+"\t\t30 Day Trend: ");
            if (day30trade >= 0)
            {
                Console.ForegroundColor = ConsoleColor.Green;
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
            }
            Console.WriteLine(day30trade+"%");
            Console.ForegroundColor = ConsoleColor.White;
        }

        private int ParsePrice(string aPrice)
        {
            int price = 0;
            while (aPrice.Contains(","))
            {
                aPrice = aPrice.Remove(aPrice.IndexOf(","), 1);
            }
            price = int.Parse(aPrice);

            return price;
        }

        private float Parse30Day(string trendChange)
        {
            float change = 0;
            int multiplier = 1;
            if (trendChange[0] != '+') //see if trend is positive or negative
                multiplier = -1;

            trendChange = trendChange.Remove(0, 1); //remove + or -
            trendChange = trendChange.Remove(trendChange.Length-1, 1); //remove %
            change = float.Parse(trendChange);
            change *= multiplier; //multiply baby

            return change;
        }
    }
}
