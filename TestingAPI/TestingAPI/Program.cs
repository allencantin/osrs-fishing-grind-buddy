﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using SimpleJSON;

namespace TestingAPI
{
    internal class Program
    {
        /// <summary>
        /// ITEM IDs
        /// raw shark 383       110XP
        /// shark 385
        /// raw swordfish 371   100XP
        /// swordfish 373
        /// raw tuna 359
        /// tuna 361        80XP
        /// raw manta ray 389
        /// manta ray 391
        /// raw bass 363    100XP
        /// bass 365
        /// raw sea turtle 395
        /// sea turtle 397
        /// raw big shark 7993  BROKEN
        /// big shark 7994      BROKEN
        /// raw big swordfish 7991  BROKEN
        /// big swordfish 7992      BROKEN
        /// </summary>
        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Fishing Grind Buddy");
            string information = "";
            string filename = "ID_Log.txt";

            List<RS_Item> fishList = new List<RS_Item>();

            StreamReader reader = new StreamReader(filename);

            Console.WriteLine("Gathering Fish Data...");
            while (reader.Peek() != -1)
            {
                string ID = reader.ReadLine();
                if(ID == null)
                {
                    break;
                }
                using (HttpClient client = new HttpClient())
                {
                    Uri apiLink = new Uri("https://secure.runescape.com/m=itemdb_oldschool/api/catalogue/detail.json?item="+ID);
                    var feedback = client.GetAsync(apiLink).Result;
                    information = feedback.Content.ReadAsStringAsync().Result;
                }
                SimpleJSON.JSONNode json = SimpleJSON.JSON.Parse(information);
                RS_Item fish = new RS_Item(json);
                //assign XP
                switch (fish.GetID())
                {
                    //raw fish
                    case 383:
                        //shark
                        fish.XP = 110;
                        fish.LevelRequirement = 76;
                        break;
                    case 371:
                        //swordfish
                        fish.XP = 100;
                        fish.LevelRequirement = 50;
                        break;
                    case 359:
                        //tuna
                        fish.XP = 80;
                        fish.LevelRequirement = 35;
                        break;
                    case 363:
                        //bass
                        fish.XP = 100;
                        fish.LevelRequirement = 46;
                        break;
                    case 389:
                        //manta ray
                        fish.XP = 69;
                        fish.LevelRequirement = 81;
                        break;
                    case 395:
                        //sea turtle
                        fish.XP = 57;
                        fish.LevelRequirement = 79;
                        break;
                    //cooked fish
                    case 385:
                        //shark
                        fish.XP = 210;
                        fish.LevelRequirement = 80;
                        break;
                    case 373:
                        //swordfish
                        fish.XP = 140;
                        fish.LevelRequirement = 45;
                        break;
                    case 361:
                        //tuna
                        fish.XP = 100;
                        fish.LevelRequirement = 30;
                        break;
                    case 365:
                        //bass
                        fish.XP = 130;
                        fish.LevelRequirement = 43;
                        break;
                    case 391:
                        //manta ray
                        fish.XP = 216.3f;
                        fish.LevelRequirement = 91;
                        break;
                    case 397:
                        //sea turtle
                        fish.XP = 211.3f;
                        fish.LevelRequirement = 82;
                        break;
                    default:
                        break;
                }
                fishList.Add(fish);
            }
            reader.Close();

            for (int i = 0; i < fishList.Count; i++)
            {
                fishList[i].DisplayItemInfo();
            }
        }
    }
}
