﻿
namespace FishingBuddy
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            // Local Notification tap event listener
            //LocalNotificationCenter.Current.NotificationActionTapped += OnNotificationActionTapped;

            MainPage = new AppShell();
        }

        //private async void OnNotificationActionTapped(NotificationActionEventArgs e) //this recieves the tap event for notifications
        //{
        //    if (e.IsTapped)
        //    {
        //        await Shell.Current.GoToAsync(nameof(ReportPage));
        //    }
        //}
    }
}
