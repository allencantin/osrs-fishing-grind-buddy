using TestingAPI;
namespace FishingBuddy;

public class ReportPage : ContentPage
{

    //thank you to Antti Koskela for this file directory find!!
    private static string mainDir = Microsoft.Maui.Storage.FileSystem.Current.AppDataDirectory;
    private static string watchlistFileName = "watchlist.txt";
    private static string filePath = System.IO.Path.Combine(mainDir, watchlistFileName);

    private string deviceToken; //for google firebase service

    List<int> watchList = new List<int>();

    private void InitializeWatchList()
    {
        filePath = System.IO.Path.Combine(mainDir, watchlistFileName);
        if (File.Exists(filePath))
        {
            StreamReader reader = new StreamReader(filePath);
            while (reader.Peek() != -1)
            {
                int ID = int.Parse(reader.ReadLine());
                watchList.Add(ID);
            }
        }
    }

    private void AssignFishProperties(ref RS_Item fish)
    {
        switch (fish.GetID())
        {
            //raw shrimps
            case 317:
                fish.XP = 10;
                fish.LevelRequirement = 1;
                break;
            //shrimps
            case 315:
                fish.XP = 30;
                fish.LevelRequirement = 1;
                break;
            //raw sardine
            case 327:
                fish.XP = 20;
                fish.LevelRequirement = 5;
                break;
            //sardine
            case 325:
                fish.XP = 40;
                fish.LevelRequirement = 1;
                break;
            //raw karambwan ji
            case 3150:
                fish.XP = 5;
                fish.LevelRequirement = 5;
                break;
            //karambwan ji
            case 3151:
                fish.XP = 10;
                fish.LevelRequirement = 1;
                break;
            //raw herring
            case 345:
                fish.XP = 30;
                fish.LevelRequirement = 10;
                break;
            //herring
            case 347:
                fish.XP = 50;
                fish.LevelRequirement = 5;
                break;
            //raw anchovies
            case 321:
                fish.XP = 40;
                fish.LevelRequirement = 15;
                break;
            //anchovies
            case 319:
                fish.XP = 30;
                fish.LevelRequirement = 1;
                break;
            //raw mackerel
            case 353:
                fish.XP = 20;
                fish.LevelRequirement = 16;
                break;
            //mackerel
            case 355:
                fish.XP = 60;
                fish.LevelRequirement = 10;
                break;
            //raw trout
            case 335:
                fish.XP = 50;
                fish.LevelRequirement = 20;
                break;
            //trout
            case 333:
                fish.XP = 70;
                fish.LevelRequirement = 15;
                break;
            //raw cod
            case 341:
                fish.XP = 45;
                fish.LevelRequirement = 23;
                break;
            //cod
            case 339:
                fish.XP = 75;
                fish.LevelRequirement = 18;
                break;
            //raw pike
            case 351:
                fish.XP = 60;
                fish.LevelRequirement = 25;
                break;
            //pike
            case 349:
                fish.XP = 80;
                fish.LevelRequirement = 20;
                break;
            //raw slimy eel
            case 3379:
                fish.XP = 80;
                fish.LevelRequirement = 28;
                break;
            //slimy eel
            case 3381:
                fish.XP = 95;
                fish.LevelRequirement = 28;
                break;
            //raw salmon
            case 331:
                fish.XP = 70;
                fish.LevelRequirement = 30;
                break;
            //salmon
            case 329:
                fish.XP = 90;
                fish.LevelRequirement = 25;
                break;
            //raw tuna
            case 359:
                fish.XP = 80;
                fish.LevelRequirement = 35;
                break;
            //tuna
            case 361:
                fish.XP = 100;
                fish.LevelRequirement = 30;
                break;
            //raw rainbow fish
            case 10138:
                fish.XP = 80;
                fish.LevelRequirement = 38;
                break;
            //rainbow fish
            case 10136:
                fish.XP = 110;
                fish.LevelRequirement = 35;
                break;
            //raw cave eel
            case 5001:
                fish.XP = 80;
                fish.LevelRequirement = 38;
                break;
            //cave eel
            case 5003:
                fish.XP = 115;
                fish.LevelRequirement = 38;
                break;
            //raw lobster
            case 377:
                fish.XP = 90;
                fish.LevelRequirement = 40;
                break;
            //lobster
            case 379:
                fish.XP = 120;
                fish.LevelRequirement = 40;
                break;
            //raw bass
            case 363:
                fish.XP = 100;
                fish.LevelRequirement = 46;
                break;
            //bass
            case 365:
                fish.XP = 130;
                fish.LevelRequirement = 43;
                break;
            //raw leaping trout
            case 11328:
                fish.XP = 50;
                fish.LevelRequirement = 48;
                break;
            //raw swordfish
            case 371:
                fish.XP = 100;
                fish.LevelRequirement = 50;
                break;
            //swordfish
            case 373:
                fish.XP = 140;
                fish.LevelRequirement = 45;
                break;
            //raw lava eel
            case 2148:
                fish.XP = 60;
                fish.LevelRequirement = 53;
                break;
            //lava eel
            case 2149:
                fish.XP = 30;
                fish.LevelRequirement = 53;
                break;
            //raw leaping salmon
            case 11330:
                fish.XP = 70;
                fish.LevelRequirement = 58;
                break;
            //raw monkfish
            case 7944:
                fish.XP = 120;
                fish.LevelRequirement = 62;
                break;
            //monkfish
            case 7946:
                fish.XP = 150;
                fish.LevelRequirement = 62;
                break;
            //raw karambwan
            case 3143:
                fish.XP = 50;
                fish.LevelRequirement = 65;
                break;
            //karambwan
            case 3144:
                fish.XP = 190;
                fish.LevelRequirement = 30;
                break;
            //raw leaping sturgeon
            case 11332:
                fish.XP = 80;
                fish.LevelRequirement = 70;
                break;
            //raw shark
            case 383:
                fish.XP = 110;
                fish.LevelRequirement = 76;
                break;
            //shark
            case 385:
                fish.XP = 210;
                fish.LevelRequirement = 80;
                break;
            //raw sea turtle
            case 395:
                fish.XP = 57;
                fish.LevelRequirement = 79;
                break;
            //sea turtle
            case 397:
                fish.XP = 211.3f;
                fish.LevelRequirement = 82;
                break;
            //raw manta ray
            case 389:
                fish.XP = 69;
                fish.LevelRequirement = 81;
                break;
            //manta ray
            case 391:
                fish.XP = 216.3f;
                fish.LevelRequirement = 91;
                break;
            //raw anglerfish
            case 13439:
                fish.XP = 120;
                fish.LevelRequirement = 82;
                break;
            //anglerfish
            case 13441:
                fish.XP = 230;
                fish.LevelRequirement = 84;
                break;
            //raw dark crab
            case 11934:
                fish.XP = 130;
                fish.LevelRequirement = 85;
                break;
            //dark crab
            case 11936:
                fish.XP = 215;
                fish.LevelRequirement = 90;
                break;
            //raw sacred eel
            case 13339:
                fish.XP = 105;
                fish.LevelRequirement = 87;
                break;

            default:
                break;
        }
    }

    private string GetImageFromID(int fishID)
    {
        switch (fishID)
        {
            //raw shrimps
            case 317:
                return "raw_shrimp.png";
            //shrimps
            case 315:
                return "shrimp.png";
            //raw sardine
            case 327:
                return "raw_sardine.png";
            //sardine
            case 325:
                return "sardine.png";
            //raw karambwan ji
            case 3150:
                return "raw_karambwanji.png";
            //karambwan ji
            case 3151:
                return "karambwanji.png";
            //raw herring
            case 345:
                return "raw_herring.png";
            //herring
            case 347:
                return "herring.png";
            //raw anchovies
            case 321:
                return "raw_anchovies.png";
            //anchovies
            case 319:
                return "anchovies.png";
            //raw mackerel
            case 353:
                return "raw_mackerel.png";
            //mackerel
            case 355:
                return "mackerel.png";
            //raw trout
            case 335:
                return "raw_trout.png";
            //trout
            case 333:
                return "trout.png";
            //raw cod
            case 341:
                return "raw_cod.png";
            //cod
            case 339:
                return "cod.png";
            //raw pike
            case 351:
                return "raw_pike.png";
            //pike
            case 349:
                return "pike.png";
            //raw slimy eel
            case 3379:
                return "raw_slimy_eel.png";
            //slimy eel
            case 3381:
                return "slimy_eel.png";
            //raw salmon
            case 331:
                return "raw_salmon.png";
            //salmon
            case 329:
                return "salmon.png";
            //raw tuna
            case 359:
                return "raw_tuna.png";
            //tuna
            case 361:
                return "tuna.png";
            //raw rainbow fish
            case 10138:
                return "raw_rainbow_fish.png";
            //rainbow fish
            case 10136:
                return "rainbow_fish.png";
            //raw cave eel
            case 5001:
                return "raw_cave_eel.png";
            //cave eel
            case 5003:
                return "cave_eel.png";
            //raw lobster
            case 377:
                return "raw_lobster.png";
            //lobster
            case 379:
                return "lobster.png";
            //raw bass
            case 363:
                return "raw_bass.png";
            //bass
            case 365:
                return "bass.png";
            //raw leaping trout
            case 11328:
                return "raw_leaping_trout.png";
            //raw swordfish
            case 371:
                return "raw_swordfish.png";
            //swordfish
            case 373:
                return "swordfish.png";
            //raw lava eel
            case 2148:
                return "raw_lava_eel.png";
            //lava eel
            case 2149:
                return "lava_eel.png";
            //raw leaping salmon
            case 11330:
                return "raw_leaping_salmon.png";
            //raw monkfish
            case 7944:
                return "raw_monkfish.png";
            //monkfish
            case 7946:
                return "monkfish.png";
            //raw karambwan
            case 3143:
                return "raw_karambwan.png";
            //karambwan
            case 3144:
                return "karambwan.png";
            //raw leaping sturgeon
            case 11332:
                return "raw_leaping_sturgeon.png";
            //raw shark
            case 383:
                return "raw_shark.png";
            //shark
            case 385:
                return "shark.png";
            //raw sea turtle
            case 395:
                return "raw_sea_turtle.png";
            //sea turtle
            case 397:
                return "sea_turtle.png";
            //raw manta ray
            case 389:
                return "raw_manta_ray.png";
            //manta ray
            case 391:
                return "manta_ray.png";
            //raw anglerfish
            case 13439:
                return "raw_angler_fish.png";
            //anglerfish
            case 13441:
                return "angler_fish.png";
            //raw dark crab
            case 11934:
                return "raw_dark_crab.png";
            //dark crab
            case 11936:
                return "dark_crab.png";
            //raw sacred eel
            case 13339:
                return "raw_sacred_eel.png";
            default:
                return "bond.png";
        }
    }

    private Grid GetFishUI(RS_Item fish)
    {
        Grid bigLayout = new Grid
        {
            RowDefinitions = { new RowDefinition() },
            ColumnDefinitions = { new ColumnDefinition(), new ColumnDefinition() }
        };

        bigLayout.BackgroundColor = Colors.Beige;
        bigLayout.Margin = 25;

        StackLayout layout1 = new StackLayout();
        layout1.Padding = 5;

        Label name = new Label();
        name.Text = fish.Name();
        name.TextColor = Colors.Black;
        name.Padding = 10;
        Label desc = new Label();
        desc.Text = fish.Description();
        desc.TextColor = Colors.Black;
        desc.Padding = 10;
        Label lvl = new Label();
        lvl.Text = "Level Requirement: " + fish.LevelRequirement.ToString();
        lvl.TextColor = Colors.Black;
        lvl.Padding = 10;
        Label xp = new Label();
        xp.Text = "XP Gained: " + fish.XP.ToString();
        xp.TextColor = Colors.Black;
        xp.Padding = 10;

        layout1.Children.Add(name);
        layout1.Children.Add(desc);
        layout1.Children.Add(lvl);
        layout1.Children.Add(xp);

        StackLayout layout2 = new StackLayout();
        layout2.Padding = 5;

        Image img = new Image();
        img.Source = GetImageFromID(fish.GetID());
        img.HeightRequest = 100;
        img.HorizontalOptions = LayoutOptions.End;

        Label price = new Label();
        price.Text = "Price: " + fish.Price().ToString();
        price.TextColor = Colors.Black;
        price.Padding = 10;
        price.HorizontalOptions = LayoutOptions.End;

        Label trend = new Label();
        trend.Text = "30 Day Trend: " + fish.Day30trade().ToString() + "%";
        if (fish.Day30trade() < 0)
            trend.TextColor = Colors.Red;
        else
            trend.TextColor = Colors.Green;
        trend.Padding = 10;
        trend.HorizontalOptions = LayoutOptions.End;

        layout2.Children.Add(img);
        layout2.Children.Add(price);
        layout2.Children.Add(trend);

        bigLayout.Add(layout1);
        bigLayout.Add(layout2, 1, 0);

        return bigLayout;
    }

    public ReportPage()
	{
        InitializeWatchList();

        string information = "";
        List<RS_Item> fishList = new List<RS_Item>();
        List<IView> UI_Elements = new List<IView>();

        for (int i = 0; i < watchList.Count; i++)
        {
            using (HttpClient client = new HttpClient())
            {
                Uri apiLink = new Uri("https://secure.runescape.com/m=itemdb_oldschool/api/catalogue/detail.json?item=" + watchList[i]);
                var feedback = client.GetAsync(apiLink).Result;
                information = feedback.Content.ReadAsStringAsync().Result;
            }
            SimpleJSON.JSONNode json = SimpleJSON.JSON.Parse(information);
            RS_Item fish = new RS_Item(json);
            //assign XP 
            AssignFishProperties(ref fish);
            fishList.Add(fish);
            UI_Elements.Add(GetFishUI(fish));
        }

        UI_Elements.Insert(0, new Label { Text = "Old School Runescape Fishing Buddy", FontSize = 24, HorizontalTextAlignment = TextAlignment.Center });
        UI_Elements.Insert(0, new Image { Source = "tiny_tempor.png", HeightRequest = 185, Aspect = Aspect.AspectFit, HorizontalOptions = LayoutOptions.Center });

        if (watchList.Count == 0)
        {
            UI_Elements.Add(new Label { Text = "\n\nGo to the All Fish page to add fish to your watchlist. They'll show up here!\n\nThe page may take a minute to load", HorizontalTextAlignment = TextAlignment.Center, VerticalTextAlignment = TextAlignment.Center });
        }

        VerticalStackLayout layout = new VerticalStackLayout();

        foreach (IView e in UI_Elements)
        {
            layout.Add(e);
        }

        ScrollView scrollView = new ScrollView();

        scrollView.Content = layout;
        Content = scrollView;


        if (Preferences.ContainsKey("DeviceToken"))
        {
            deviceToken = Preferences.Get("DeviceToken", "");
        }
        //may i say thank GOD for ancient forum posts for saving my life from the JAVA0000 error after having done the firebase service thing
        //source: https://developercommunity.visualstudio.com/t/JAVA0000-upgrading-Maui-app-to--net-8-a/10534242?q=jira&sort=newest

    }
}