﻿using Android.App;
using Android.Content;
using AndroidX.Core.App;
using Firebase.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;

namespace FishingBuddy.Platforms.Android.Services
{
    [Service(Exported=true)]
    [IntentFilter(new[] {"com.google.firebase.MESSAGING_EVENT"})]
    public class FirebaseService : FirebaseMessagingService
    {
        public FirebaseService()
        {
            
        }

        public override void OnNewToken(string token)
        {
            base.OnNewToken(token);
            if (Preferences.ContainsKey("DeviceToken"))
            {
                Preferences.Remove("DeviceToken");
            }
            Preferences.Set("DeviceToken", token);
        }

        public override void OnMessageReceived(RemoteMessage message)
        {
            base.OnMessageReceived(message);

            var notification = message.GetNotification();

            DisplayNotification(notification.Body, notification.Title, message.Data);
        }

        private void DisplayNotification(string body, string title, IDictionary<string, string> data)
        {
            //get navigation data

            //intent for notification actions?
            Intent intent = new Intent(this, typeof(MainActivity));
            intent.AddFlags(ActivityFlags.ClearTop);

            foreach(string key in data.Keys)
            {
                string value = data[key];
                intent.PutExtra(key, value);
            }

            var pendingNav = PendingIntent.GetActivity(this,
                MainActivity.NotificationID, intent, PendingIntentFlags.OneShot);

            //getting the data and packaging it as a notification
            var _notificationBuilder = new NotificationCompat.Builder(this, MainActivity.ChannelID)
                .SetContentTitle(title)
                .SetSmallIcon(Resource.Mipmap.buddy)
                .SetContentText(body)
                .SetChannelId(MainActivity.ChannelID)
                .SetContentIntent(pendingNav);

            var _notificationManager = NotificationManagerCompat.From(this);

            //this will show the notification
            _notificationManager.Notify(MainActivity.NotificationID, _notificationBuilder.Build());

        }
    }
}
