﻿using Android.App;
using Android.Content.PM;
using Android.OS;

namespace FishingBuddy
{
    [Activity(Theme = "@style/Maui.SplashTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation | ConfigChanges.UiMode | ConfigChanges.ScreenLayout | ConfigChanges.SmallestScreenSize | ConfigChanges.Density)]
    public class MainActivity : MauiAppCompatActivity
    {
        internal static readonly string ChannelID = "TestChannel";
        internal static readonly int NotificationID = 413;
        protected override void OnCreate(Bundle? savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            //taking care of navigation from notification
            if (Intent.Extras != null)
            {
                foreach (var key in Intent.Extras.KeySet())
                {
                    if (key == "NavigationID")
                    {
                        if (Preferences.ContainsKey(key))
                        {
                            Preferences.Remove(key);
                        }
                        Preferences.Set("NotificationID", "Report");
                    }
                }
            }

            CreateNotificationChannel();
        }
        //notification channel for when the notification is sent, the app can display it
        private void CreateNotificationChannel()
        {
            if (OperatingSystem.IsOSPlatformVersionAtLeast("android", 26)) //notification channel only on android 26+
            {
                NotificationChannel channel = new NotificationChannel(ChannelID, "Test Notification", NotificationImportance.Default);

                //gets system notification manager
                var _notificationManager = (NotificationManager)GetSystemService(Android.Content.Context.NotificationService);

                //system creates channel for notifications for this app
                _notificationManager.CreateNotificationChannel(channel);
            }

            
        }
    }
}
